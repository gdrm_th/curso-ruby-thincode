puts "Hello world"
puts "foo" + "bar"
puts "arriba \n los pumas"
nombre = "Gerardo"
puts nombre.class
puts "Mi nombre es #{nombre.length}"
puts "foo" + 1.to_s
puts 'foo' + 'bar #{nombre}'

nombre = 1
print nombre.class
pass = "passsssss"
if pass.length > 8 
  puts "el pass es correcto"
else
  puts "El pass es muy corto"
end
variable = 2
puts "mayor" if variable > 3

puts !!1
puts !!""
puts !!nil
puts !!false

if !variable
  puts "correcto"
else
  puts "incorrecto"
end

unless variable
  puts "correcto"
else
  puts "incorrecto"
end

puts "incorrecto" unless !variable

puts "".empty?
puts "Juan".downcase
puts "juan".upcase
puts "gerardo".include?("ge")

nombre2 = "Daniel"
puts nombre2[0]
for i in 0..5
    puts nombre2[i]
end
(0..5).each do |i|
  puts nombre2[i]
end

(0..nombre2.length - 1).each {|i| puts nombre2[i]}
nombre2.split("").each {|i| puts nombre2[i]}

arreglo = ["j", "u", "a", "n"]
puts arreglo[0]
puts arreglo[100].inspect
puts !!arreglo[1000]

arreglo.each do |elem|
  puts elem    
end

arreglo2 = [1, 4, 3]
arreglo2.each do |elem|
    puts elem    
end
puts 1.to_s + "juan" + true.to_s
puts true.methods
puts arreglo2.join

arreglo.push("jose")
puts arreglo.inspect
puts arreglo.pop(2)
puts arreglo.inspect
arreglo[-1]
arreglo.last
arreglo.reverse
puts "de aqui pa bajo"
puts arreglo[-10].inspect
arreglo3 = []
puts arreglo3.inspect
puts arreglo3.class
arreglo3 = 1
puts arreglo3.inspect
puts arreglo3.class

puts arreglo2.sort.inspect
puts arreglo2.reverse.inspect
puts arreglo2.inspect

puts arreglo2.sort!.inspect
puts arreglo2.reverse!.inspect
puts arreglo2.inspect

#Time
today = Time.new
puts today
born_day= Time.new(1998,4,3,12,35,20)
puts born_day
puts today.month
puts today.day
puts today.wday
puts today.to_f

#regexp
zip_code = /\d{5}/
puts zip_code.class
puts zip_code.inspect
puts "martinica 37500 silao 36100 gto 36258".match(zip_code).inspect
puts "martinica 37500 silao 36100 gto 36258".scan(zip_code).inspect
puts "martinica 37500 silao 36100 gto 36258".split(zip_code)

user = {}
puts user.class
user["name"] = "Gerardo"
user["pass"] = "foo"
puts user.inspect
user2 = { "name"=>"carlos", "pass"=>"bar"  }
user3 = { :name=>"Julio", :pass=>"bin" }
user4 = { name:"Mari", pass:"son", email:"mari@corre.com"}

puts user4[:name]
puts user4["noexiste"].inspect

user4.each do |key, value|
  puts "key:#{key} value:#{value}"
end

puts user4.length

def hamburguesa
  puts "pan"
  yield "queso", "carne"
  puts "pan"
end

hamburguesa do |i, y| 
  puts i
  puts y
  puts "lechuga"
end

equipos = ["Necaxa", "Cruz Azul", "Pumas", "Lobos BUAP", "Chives"]

def normalize_for_url(equipos)
  equipos_url = []
  equipos.each do |equipo|
    equipos_url << equipo.downcase.split.join("-")
  end
  equipos_url
end

puts normalize_for_url(equipos).inspect

def normalize_for_url_map(equipos)
  equipos.map do |equipo|
    equipo.downcase.split.join("-")
  end
end

puts normalize_for_url_map(equipos).inspect

def split_array_teams(equipos)
  equipos_new = []
  equipos.each do |equipo|
    equipos_new << equipo if equipo.split(/\s+/).length > 1
  end
  equipos_new
end

puts split_array_teams(equipos).inspect

def split_array_teams_2(equipos)
  equipos.select do |equipo|
    equipo.split(/\s+/).length > 1
  end
end

puts split_array_teams_2(equipos)
puts "******CLASES*******"
def poligono(nombre, *puntos)
end

def user(name, pass="pass")
end


#class
#initialize
#getters y setters

class Phrase
  def initialize(content)
    #@@variable_clase = 1
    @content = content
  end
  attr_accessor :content
  #attr_reader :content
  #attr_writer :content
  protected
  def palindrome?
    self.content.downcase == self.content.downcase.reverse
  end




end

class TranslatedPhrase < Phrase
  
  def initialize(content, translation)
    super(content)
    @translation = translation
  end
  attr_accessor :translation

  def palindrome?
    self.translation.downcase == self.translation.downcase.reverse
  end

  def to_s
    self.translation.upcase
  end
end

frase = Phrase.new("esto es una frase")
puts frase.inspect
frase2 = Phrase.new("esto es una frase")
puts frase2.inspect
puts frase.content  
frase3 = Phrase.new("juan")
#puts frase3.palindrome?
frase4 = TranslatedPhrase.new("recognize", "reconocer")
puts frase4.palindrome?
puts frase4.class
puts frase4.class.superclass
puts frase4.class.superclass.superclass
puts frase4.class.superclass.superclass.superclass
puts frase4.class.superclass.superclass.superclass.superclass.inspect 
=begin
class User
  def initialize(name, pass = "pass")
  end
end

user = {name: "gerardo", pass:"pass"}
class User
  def initialize(user)
    @nombre = user[:name]
  end
end

user = User.new("Gerard")
class User
  def initialize(*user)
    
  end
end
=end

class String
  def palindrome?
    self.downcase == self.downcase.reverse
  end
end



puts "racecar".palindrome?

