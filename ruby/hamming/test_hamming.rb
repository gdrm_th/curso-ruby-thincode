require 'minitest/autorun'
require_relative 'hamming'

# Common test data version: 2.1.0 b5d154b
class HammingTest < Minitest::Test
  def test_empty_strands
    #skip
    assert_equal 0, compute_hamming('', '')
  end

  def test_identical_strands
    skip
    assert_equal 0, compute_hamming('A', 'A')
  end

  def test_long_identical_strands
    skip
    assert_equal 0, compute_hamming('GGACTGA', 'GGACTGA')
  end

  def test_complete_distance_in_single_nucleotide_strands
    skip
    assert_equal 1, compute_hamming('A', 'G')
  end

  def test_complete_distance_in_small_strands
    skip
    assert_equal 2, compute_hamming('AG', 'CT')
  end

  def test_small_distance_in_small_strands
    skip
    assert_equal 1, compute_hamming('AT', 'CT')
  end

  def test_small_distance
    skip
    assert_equal 1, compute_hamming('GGACG', 'GGTCG')
  end

  def test_small_distance_in_long_strands
    skip
    assert_equal 2, compute_hamming('ACCAGGG', 'ACTATGG')
  end

  def test_non_unique_character_in_first_strand
    skip
    assert_equal 1, compute_hamming('AAG', 'AAA')
  end

  def test_non_unique_character_in_second_strand
    skip
    assert_equal 1, compute_hamming('AAA', 'AAG')
  end

  def test_same_nucleotides_in_different_positions
    skip
    assert_equal 2, compute_hamming('TAG', 'GAT')
  end

  def test_large_distance
    skip
    assert_equal 4, compute_hamming('GATACA', 'GCATAA')
  end

  def test_large_distance_in_off_by_one_strand
    skip
    assert_equal 9, compute_hamming('GGACGGATTCTG', 'AGGACGGATTCT')
  end

  def test_disallow_first_strand_longer
    skip
    result = compute_hamming('AAAG', 'AAG')
    refute result, "Expected false, the two strings must have the same size"
  end

  def test_disallow_second_strand_longer
    skip
    result = compute_hamming('AAA', 'AATG')
    refute result, "Expected false, the two strings must have the same size"
  end
end